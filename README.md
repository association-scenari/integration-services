## Intégration des services

Ce dépôt contient des scripts utiles pour renforcer l'intégration des services offerts par l'association Scenari. Ils sont néanmoins génériques et s'appuient sur les APIs des services sous-jacents.

## Prérequis

### Dépendances 

Python > 3.8. Installer les dépendances :

```sh
pip install -r requirements.txt
```

### Génération d'une clé d'API Dolibarr

Les clés d'API Dolibarr sont liées à un compte.

Se rendre sur sa page de profil Dolibarr, onglet `Utilisateur`. Cliquer sur `Modifier`, identifier le champ `Clé pour l'API` et cliquer sur le bouton en forme de flèche circulaire pour générer une clé. Copier la clé et enregistrer.

> ℹ️ L'API Dolibarr peut être explorée ici : https://gestion.scenari.org/api/index.php/explorer/#/login

### Génération d'une clé d'API Discourse

Les clés d'API Discourse peuvent être liées à un compte ou au niveau global. Pour des raisons d'audit et de sécurité, il est préférable de les lier à un compte unique.

Se rendre sur [le panel d'administration](https://forums.scenari.org/admin/api/keys/new) et générer une clé de type « Utilisateur unique ».

Autoriser les appels suivants :
- `users` : show, check emails, update, list
- `badges` : list user badges, assign badges to user, revoke badge from user

Copier la clé et enregistrer.

> ℹ️ L'API Discourse peut être explorée ici : https://docs.discourse.org/

## Utilisation

### Utilisation générique

Les scripts sont tous lançables à la main.

Il est possible de construire une image Docker prête à l'emploi :

```sh
docker build -t sc-integ .
```

L'utilisation des scripts est identique avec Docker (on passera la commande au conteneur) ou sans.

Il est aussi imaginable d'utiliser un fichier Compose pour faciliter la configuration, par exemple :

```yaml
services:
  umap:
    image: scenari-integ
    build: .
    container_name: integ_dolibarr_umap
    environment:
      DOLIBARR_INSTANCE_URL: XXX
      DOLIBARR_API_KEY: XXX
    command: ./generate_map.py
```

puis :

```sh
docker compose build && docker compose up -d && docker compose logs -f
```

Le lancement, conteneurisé ou non, peut se mettre dans un `cron` pour tourner périodiquement.

### Générer une carte des adhérent·es

Objectif : générer une carte interactive des adhérent·es Dolibarr à l'aide de uMap. Comme uMap n'a pas d'API, l'idée est surtout de générer un fichier [GeoJSON](https://en.wikipedia.org/wiki/GeoJSON) qui pourra être importé par la suite. L'idée est de transformer les adresses en coordonnées GPS grâce à [Nominatim](https://nominatim.openstreetmap.org).

```
usage: generate_map.py [-h] [-c COUNTRIES] [-x EXCLUDE] [-l LOG_LEVEL]

Generates a map which show the locations where active members of a Dolibarr instance live.

optional arguments:
  -h, --help            show this help message and exit
  -c COUNTRIES, --countries COUNTRIES
                        List of ISO 3166 countries codes or 'all' to include, separated by commas (default: all)
  -x EXCLUDE, --exclude EXCLUDE
                        List of ISO 3166 countries codes or 'all' to exclude, separated by commas (default: none)
  -l LOG_LEVEL, --log-level LOG_LEVEL
                        configure the logging level (DEBUG, INFO, WARNING, or ERROR)

Instances configuration is passed through environment variables.
* DOLIBARR_INSTANCE_URL is the base URL of your Dolibarr instance
* DOLIBARR_API_KEY is the API key for accessing your Dolibarr members
* (optional) UMAP_PRIVATE_URL is an editable private URL of an existing map
* (optional) UMAP_NAME is the name of the map

On the map, colors are represent the precision level of geocoding.
* Green means town level
* Blue means county level
* Red means country level
```

### Assigner un groupe Discourse aux adhérent·es

Objectif : assigner automatique un groupe Discourse à l'ensemble des personnes adhérentes sur Dolibarr, ainsi qu'un badge indiquant le statut d'adhésion.

```
usage: à venir
```

### Rapatrier le pseudo Discourse sur Dolibarr

Le matching se fait par email. 

```
usage: sync_pseudos.py [-h] [-l LOG_LEVEL]

Sets « Pseudo forum » field on Dolibarr for each matching user on Discourse.

options:
  -h, --help            show this help message and exit
  -l LOG_LEVEL, --log-level LOG_LEVEL
                        configure the logging level (DEBUG, INFO(*), WARNING, or ERROR)

Instances configuration is passed through environment variables.
* DOLIBARR_INSTANCE_URL is the base URL of your Dolibarr instance
* DOLIBARR_API_KEY is the API key for accessing your Dolibarr members
* DISCOURSE_INSTANCE_URL is the base URL of your Discourse instance
* DOLIBARR_API_KEY is the API key for accessing your Discourse users
```