"""
This module contains generic classes which can be inherited
to factorize some useful boilerplate code when coding API
clients. Exemple are automatic handling of bad HTTP code,
management of a session so that cookies are persisted,
automatic addition of an authentication header, and so on.
"""

from typing import Dict, List, Set

from requests import Response, Session


class APIFailure(Exception):
    """Base exception that indicates something went wrong during an API call."""

    def __init__(self, method: str, endpoint: str, response: Response):
        self.method = method
        self.endpoint = endpoint
        self.response = response
        super().__init__(
            f"Bad return code {response.status_code} for {method} on endpoint {endpoint}. Error: {response.text}"
        )


class APISession(Session):
    """
    Base API Session Handler.
    Wrapper around Session to set the base API URL,
    throw an exception on unexpected return code
    and add optional authentication headers.
    """

    # Default values inspired by RFC9110
    CORRECT_STATUS_CODE: Dict[str, Set[int]] = {
        "GET": {200},
        "HEAD": {200},
        "POST": {200, 201, 202, 203, 204, 205},
        "PUT": {200, 201, 204},
        "DELETE": {200, 202, 204},
        "OPTIONS": {200},
    }

    def __init__(
        self,
        base_url: str,
        base_api_fragment: str = None,
        *,
        headers: Dict[str, str] = None,
        additional_status_code: Dict[str, List[int]] = None,
        **kwargs,
    ):
        """
        Initialize the underlying Session with custom params.

        base_url is the URL of your service, no trailing slash.
        base_api_fragment is the prefix of API calls, no trailing slash.
        headers is an optional Dictionary with additional headers (e.g. needed to authenticate).

        additional_status_code is a dict formatted with {HTTP_METHOD: [status_code...]}.
        By default the API session expects the service's API to comply to RFC9110
        and have reasonable defaults. If the API can send a `201 Created` for a GET method,
        which is bad, please pass a dict like {'GET': [201]}.
        """
        super().__init__(**kwargs)
        self._api_url = base_url
        if base_api_fragment:
            self._api_url = f'{self._api_url}/{base_api_fragment}'
        if headers:
            self.headers.update(headers)
        if additional_status_code:
            for method, codes in additional_status_code.items():
                self.CORRECT_STATUS_CODE[method].update(set(codes))

    def request(self, method: str, url: str, *args, **kwargs) -> Response:
        """
        Call the API with the configured session.

        url is to be understood as an endpoint of the API.
        """
        url = f'{self._api_url}/{url.lstrip("/")}'
        response = super().request(method, url, *args, **kwargs)
        if response.status_code not in self.CORRECT_STATUS_CODE[method]:
            raise APIFailure(method, url, response)
        return response
