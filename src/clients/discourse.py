"""
Discourse-related utilities
"""

import logging
from enum import Enum
from .api import APISession


class DiscourseUserFlag(Enum):
    """Strings in Discourse to designate user status"""
    ACTIVE = "active"
    NEW = "new"
    STAFF = "staff"
    SUSPENDED = "suspended"
    BLOCKED = "blocked"
    SUSPECT = "suspect"


class DiscourseClient:
    """
    A wrapper for a subset of the Discourse API.
    """

    HEADER_API_KEY = "Api-Key"
    HEADER_API_USERNAME = "Api-Username"

    def __init__(
        self,
        instance_url: str,
        api_token: str = None,
        api_username: str = None
    ):
        """
        Initialize a session with Discourse with optional authentication.

        instance_url: base instance of Discourse
        api_token: optional generated token (see <instance_url>/admin/api/keys)
        api_username: optional, if your token is bound to an account (can't find a way to make it work)
        """
        # We want to force the API to send JSON
        headers = {'Accept': 'application/json'}
        if api_token:
            headers[self.HEADER_API_KEY] = api_token
            if api_username:
                headers[self.HEADER_API_USERNAME] = api_username
            else:
                headers[self.HEADER_API_USERNAME] = 'system'

        self._session = APISession(instance_url, headers=headers)

    def _get_users(self, page: int, flag: DiscourseUserFlag, params: dict=None) -> dict:
        """Return a single page of users with email included."""
        data = {"show_emails": "true", "page": page}
        if params is not None:
            data |= params
        logging.debug(data)
        return self._session.get(f"admin/users/list/{flag}", params=data).json()

    def get_active_users(self, params: dict=None):
        """
        Return all active members.

        :param See https://docs.discourse.org/#tag/Users/operation/adminListUsers
        """
        all_active_members: list[dict] = []
        page: int = 1
        if not params:
            params = {}
        current_page: list[dict] = self._get_users(page, DiscourseUserFlag.ACTIVE.value, params)
        while current_page:
            all_active_members.extend(current_page)
            page = page + 1
            current_page = self._get_users(page, DiscourseUserFlag.ACTIVE.value, params)
        return all_active_members

    def get_active_users_by_email(self, email: str):
        """Helper method to get all users with a given primary or secondary email"""
        active_members = self.get_active_users()
        matching_users = [
            m
            for m in active_members
            if email == m["email"] or email in m["secondary_emails"]
        ]
        return matching_users

    def get_groups_by_name(self, group_name: str) -> dict:
        """Helper method to get groups matching a name"""
        groups = self._session.get("groups").json()
        return [g for g in groups if g["name"] == group_name]


    def deactivate_user(self, id: int):
        self._session.put(f"admin/users/{id}/deactivate")

    def remove_group_members(self, group_id: int, usernames: list[str]) -> dict:
        """
        Remove a member from a group.

        Returns success status and username deletion status
        See https://docs.discourse.org/#tag/Groups/operation/removeGroupMembers
        """
        data = {"username": ",".join(usernames)}
        return self._session.delete(f"groups/{group_id}/members", data=data).json()

    def add_group_members(self, group_id: int, usernames: list[str]) -> dict:
        """
        Remove a member from a group.

        Returns success status
        See https://docs.discourse.org/#tag/Groups/operation/addGroupMembers
        """
        data = {"username": ",".join(usernames)}
        return self._session.put(f"groups/{group_id}/members", data=data).json()

    def get_badges(self) -> dict:
        """Get all badges, including custom badges."""
        return self._session.get("admin/badges").json()["badges"]

    def get_badge_id(self, badge_name: str) -> dict:
        """
        Get a badge identifier from its name.
        Assumes no duplicates.
        """
        badges = self.get_badges()
        badge_ids = [b['id'] for b in badges if b['name'] == badge_name]
        if not badge_ids:
            raise ValueError(f"No badge with name {badge_name} found.")
        if len(badge_ids) > 1:
            logging.warning(f'Multiple badges with name {badge_name}, returning first id.')
        return badge_ids[0]

    def grant_custom_badge(
        self, username: str, badge_id: int, *, reason: str = None
    ) -> dict:
        """
        Grant a custom badge to a user.

        reason is optional and must be a topic or post URL

        Returns details about the badge.
        See https://meta.discourse.org/t/grant-a-custom-badge-through-the-api/103270
        """
        data = {"username": username, "badge_id": badge_id, "reason": reason}
        return self._session.post('user_badges', data=data).json()

    def deny_custom_badge(self, username: str, badge_id: int) -> dict:
        """Remove a custom badge of a user."""
        data = {"username": username, "badge_id": badge_id}
        return self._session.delete('user_badges', data=data).json()