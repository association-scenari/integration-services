"""
Module to interact with uMap, an OpenStretMap service
allowing to draw on top of a map.

uMap does not offer a proper API as everying basically works
in JS, in a browser. However, there is a few features
(login, import of GeoJSON) that can be made through
simple HTTP requests.

The intented goal of this module is, for now, essentially
the import of a GeoJSON file.

Additionnaly, this module is able to add options to a
vanilla GeoJSON file to control appearance of POIs (Point of Interests).

It is not intented to be exhaustive. It has not been tested with
multiple layers.
"""

import logging

from dataclasses import dataclass
from enum import Enum
from json import loads as json_load, dumps as json_dumps
from os.path import basename
from re import search
from typing import Dict, List, Tuple

import requests
from geojson import (
    loads as geojson_load,
    dumps as geojson_dumps,
    Feature,
    FeatureCollection,
    Point,
)
from simplejson.errors import JSONDecodeError

from .api import APISession


class uMapAnonymousEdit(Enum):
    ANYONE = 1
    PRIVATE = 3


class uMapIcon(Enum):
    DEFAULT = "Default"
    CIRCLE = "Circle"
    DROP = "Drop"
    BALL = "Ball"


class uMapColor(Enum):
    BLACK = "Black"
    NAVY = "Navy"
    DARKBLUE = "DarkBlue"
    MEDIUMBLUE = "MediumBlue"
    BLUE = "Blue"
    DARKGREEN = "DarkGreen"
    GREEN = "Green"
    TEAL = "Teal"
    DARKCYAN = "DarkCyan"
    DEEPSKYBLUE = "DeepSkyBlue"
    DARKTURQUOISE = "DarkTurquoise"
    MEDIUMSPRINGGREEN = "MediumSpringGreen"
    LIME = "Lime"
    SPRINGGREEN = "SpringGreen"
    AQUA = "Aqua"
    CYAN = "Cyan"
    MIDNIGHTBLUE = "MidnightBlue"
    DODGERBLUE = "DodgerBlue"
    LIGHTSEAGREEN = "LightSeaGreen"
    FORESTGREEN = "ForestGreen"
    SEAGREEN = "SeaGreen"
    DARKSLATEGRAY = "DarkSlateGray"
    DARKSLATEGREY = "DarkSlateGrey"
    LIMEGREEN = "LimeGreen"
    MEDIUMSEAGREEN = "MediumSeaGreen"
    TURQUOISE = "Turquoise"
    ROYALBLUE = "RoyalBlue"
    STEELBLUE = "SteelBlue"
    DARKSLATEBLUE = "DarkSlateBlue"
    MEDIUMTURQUOISE = "MediumTurquoise"
    INDIGO = "Indigo"
    DARKOLIVEGREEN = "DarkOliveGreen"
    CADETBLUE = "CadetBlue"
    CORNFLOWERBLUE = "CornflowerBlue"
    MEDIUMAQUAMARINE = "MediumAquaMarine"
    DIMGRAY = "DimGray"
    DIMGREY = "DimGrey"
    SLATEBLUE = "SlateBlue"
    OLIVEDRAB = "OliveDrab"
    SLATEGRAY = "SlateGray"
    SLATEGREY = "SlateGrey"
    LIGHTSLATEGRAY = "LightSlateGray"
    LIGHTSLATEGREY = "LightSlateGrey"
    MEDIUMSLATEBLUE = "MediumSlateBlue"
    LAWNGREEN = "LawnGreen"
    CHARTREUSE = "Chartreuse"
    AQUAMARINE = "Aquamarine"
    MAROON = "Maroon"
    PURPLE = "Purple"
    OLIVE = "Olive"
    GRAY = "Gray"
    GREY = "Grey"
    SKYBLUE = "SkyBlue"
    LIGHTSKYBLUE = "LightSkyBlue"
    BLUEVIOLET = "BlueViolet"
    DARKRED = "DarkRed"
    DARKMAGENTA = "DarkMagenta"
    SADDLEBROWN = "SaddleBrown"
    DARKSEAGREEN = "DarkSeaGreen"
    LIGHTGREEN = "LightGreen"
    MEDIUMPURPLE = "MediumPurple"
    DARKVIOLET = "DarkViolet"
    PALEGREEN = "PaleGreen"
    DARKORCHID = "DarkOrchid"
    YELLOWGREEN = "YellowGreen"
    SIENNA = "Sienna"
    BROWN = "Brown"
    DARKGRAY = "DarkGray"
    DARKGREY = "DarkGrey"
    LIGHTBLUE = "LightBlue"
    GREENYELLOW = "GreenYellow"
    PALETURQUOISE = "PaleTurquoise"
    LIGHTSTEELBLUE = "LightSteelBlue"
    POWDERBLUE = "PowderBlue"
    FIREBRICK = "FireBrick"
    DARKGOLDENROD = "DarkGoldenRod"
    MEDIUMORCHID = "MediumOrchid"
    ROSYBROWN = "RosyBrown"
    DARKKHAKI = "DarkKhaki"
    SILVER = "Silver"
    MEDIUMVIOLETRED = "MediumVioletRed"
    INDIANRED = "IndianRed"
    PERU = "Peru"
    CHOCOLATE = "Chocolate"
    TAN = "Tan"
    LIGHTGRAY = "LightGray"
    LIGHTGREY = "LightGrey"
    THISTLE = "Thistle"
    ORCHID = "Orchid"
    GOLDENROD = "GoldenRod"
    PALEVIOLETRED = "PaleVioletRed"
    CRIMSON = "Crimson"
    GAINSBORO = "Gainsboro"
    PLUM = "Plum"
    BURLYWOOD = "BurlyWood"
    LIGHTCYAN = "LightCyan"
    LAVENDER = "Lavender"
    DARKSALMON = "DarkSalmon"
    VIOLET = "Violet"
    PALEGOLDENROD = "PaleGoldenRod"
    LIGHTCORAL = "LightCoral"
    KHAKI = "Khaki"
    ALICEBLUE = "AliceBlue"
    HONEYDEW = "HoneyDew"
    AZURE = "Azure"
    SANDYBROWN = "SandyBrown"
    WHEAT = "Wheat"
    BEIGE = "Beige"
    WHITESMOKE = "WhiteSmoke"
    MINTCREAM = "MintCream"
    GHOSTWHITE = "GhostWhite"
    SALMON = "Salmon"
    ANTIQUEWHITE = "AntiqueWhite"
    LINEN = "Linen"
    LIGHTGOLDENRODYELLOW = "LightGoldenRodYellow"
    OLDLACE = "OldLace"
    RED = "Red"
    FUCHSIA = "Fuchsia"
    MAGENTA = "Magenta"
    DEEPPINK = "DeepPink"
    ORANGERED = "OrangeRed"
    TOMATO = "Tomato"
    HOTPINK = "HotPink"
    CORAL = "Coral"
    DARKORANGE = "DarkOrange"
    LIGHTSALMON = "LightSalmon"
    ORANGE = "Orange"
    LIGHTPINK = "LightPink"
    PINK = "Pink"
    GOLD = "Gold"
    PEACHPUFF = "PeachPuff"
    NAVAJOWHITE = "NavajoWhite"
    MOCCASIN = "Moccasin"
    BISQUE = "Bisque"
    MISTYROSE = "MistyRose"
    BLANCHEDALMOND = "BlanchedAlmond"
    PAPAYAWHIP = "PapayaWhip"
    LAVENDERBLUSH = "LavenderBlush"
    SEASHELL = "SeaShell"
    CORNSILK = "Cornsilk"
    LEMONCHIFFON = "LemonChiffon"
    FLORALWHITE = "FloralWhite"
    SNOW = "Snow"
    YELLOW = "Yellow"
    LIGHTYELLOW = "LightYellow"
    IVORY = "Ivory"
    WHITE = "White"


@dataclass
class uMapFeature:
    """
    Helper class based on to represent a GeoJSON feature
    on uMap with extra properties.

    These properties are far to be exhaustive and some could be added from
    there : https://github.com/umap-project/umap/blob/9b0986c16b8175af37df3a1001d15f0306d42197/umap/static/umap/js/umap.js#L1027
    """
    feature: Feature
    icon: uMapIcon = uMapIcon.DEFAULT
    color: uMapColor = uMapColor.BLUE
    show_label: bool = True

@dataclass
class uMapSettings:
    """Unexhausive settings that can be changed with this client"""

    map_name: str = None
    map_center: Tuple[float, float] = None

    # This method is from dataclasses. We want to avoid
    # None even is explicitely passed and fall back to
    # default values, because the API won't authorize None.
    def __post_init__(self):
        if self.map_name is None:
            self.map_name = "Untitled map"
        if self.map_center is None:
            self.map_center = (2.0, 51.0)


class uMapAPIFailure(Exception):
    """Exception raised when uMap returns a JSON object with an error key."""

    def __init__(self, method: str, endpoint: str, error: str):
        super().__init__(
            f"Error detected in the answer of {method} on endpoint {endpoint} : {error}."
        )


class uMapSession(APISession):
    """
    uMap has a "strange" API mixing 200 OK with JSON errors and HTTP standard error codes.
    Additionnally it requires certain API endpoints to end with a `/` to recognize them.

    It also needs a CSRF token in headers and a valid Referer header.
    To separate this technical stuff from map logic, this class wraps APISession and handle all this.
    """

    # API-path related stuff
    BASE_API_FRAGMENT = "en/map"
    _PRIVATE_MAP_FRAGMENT = "anonymous-edit"

    # CSRF-related field names
    _CSRF_HEADER = "X-CSRFToken"
    _CSRF_COOKIE = "csrftoken"

    _ERRORS_KEY = "errors"

    def __init__(
        self,
        base_url: str,
        **kwargs,
    ):
        super().__init__(
            base_url,
            self.BASE_API_FRAGMENT,
            # uMap can legitimally redirect private edit links
            additional_status_code={"GET": [302]},
            **kwargs,
        )
        # Needed to validate CSRF protection
        self.headers["Referer"] = base_url

    def request(self, method: str, url: str, *args, **kwargs) -> requests.Response:
        response = super().request(method, url, *args, **kwargs)
        # CSRF not set in private link redirections
        if self._PRIVATE_MAP_FRAGMENT not in url:
            self._set_csrf_header()
        # Try to identify if answer is JSON and check for errors
        try:
            json = response.json()
            if error := json.get(self._ERRORS_KEY):
                raise uMapAPIFailure(method, url, error)
        except JSONDecodeError:
            pass
        return response

    def _set_csrf_header(self) -> None:
        """
        Sets CSRF header from cookie for the whole session.

        CSRF generally prevents an attacker from forging a request
        sent from another website, e.g. in a JS script, by forcing
        requests to contain a specific value which has been set
        as a cookie in a previous GET request.

        Thus, a previous GET request is needed so this method works.
        """
        if (csrf_token := self.cookies.get(self._CSRF_COOKIE)) is not None:
            if csrf_token != self.headers.get(self._CSRF_HEADER):
                logging.debug("New CSRF token found")
                self.headers[self._CSRF_HEADER] = csrf_token
        else:
            logging.warning("No CSRF token found, further requests will probably fail")


@dataclass
class uMapLayer:
    """Represents the properties of a layer in uMap."""

    name: str
    identifier: int
    displayOnLoad: bool


class uMapClient:
    """
    Tries to emulate a uMap API.

    We don't propose login feature because it uses
    exclusively OAuth2 and it too annoying to implement
    for such a specific task.

    Each instance is supposed to be bound to a single map.
    """

    DEFAULT_INSTANCE = "https://umap.openstreetmap.fr"
    # Default web UI values.
    # TODO check if it is necessary to send all of those
    # to create a map, or if there is defaults server-side.
    DEFAULT_PROPERTIES = {
        "easing": True,
        "embedControl": True,
        "fullscreenControl": True,
        "searchControl": True,
        "datalayersControl": True,
        "zoomControl": True,
        "slideshow": {},
        "captionBar": False,
        "limitBounds": {},
        "licence": "",
        "description": "",
        "displayPopupFooter": False,
        "miniMap": False,
        "moreControl": True,
        "scaleControl": True,
        "scrollWheelZoom": True,
        "zoom": 6,
    }

    # Match key "datalayer" with an array of objects,
    # potentially on several lines, in JSON syntax
    _LAYERS_REGEXP = r'"datalayers": (\[(\n|\r|.)+\])'

    # Regexp for catching anonymous edit permissions
    _EDIT_STATUS_REGEXP = r'"edit_status": (\d),'

    # Pretty bad regexp to catch a portion of settings of a map, need to close an extra }
    _PROPERTIES_REGEXP = r'"properties": ({(\n|\r|.)+}),(\n|\r)+"urls":'

    # Regexp to catch the map center
    _CENTER_REGEXP = r'"coordinates": (\[(\n|\r|.)+?\])'

    # Extract instance URL and ID from a private URL, normally there → "<url>/en/map/anonymous-edit/<id>:[...]"
    _ID_REGEXP = rf"(.+)/{uMapSession.BASE_API_FRAGMENT}/anonymous-edit/([0-9]+):"

    def __init__(self, private_url: str):
        """
        Initialize a uMap Client from an existing map.

        private_url is necessary if you want to edit an already existant private map.
        This URL is to be found in the web UI (key icon) or in stdout there.

        Caution : HTTPS will be forced.
        """
        if len(fragments := private_url.split(uMapSession.BASE_API_FRAGMENT)) > 1:
            self._private_fragment = fragments[-1].strip("/")
            self._instance_url = fragments[0].strip("/").replace("http://", "https://")
        else:
            raise ValueError(
                f"Private URL does not contain {uMapSession.BASE_API_FRAGMENT} but is supposed to."
            )

        # New session with CSRF and cookie-related stuff
        self._session = uMapSession(self._instance_url)

        # If map is editable only through private link,
        # we need to visit if first to set some cookies.
        # Then, the API can be called with the map ID and
        # no additional parameter.
        r = self._session.get(self._private_fragment, allow_redirects=False)
        # If the private link is valid, uMap answers with a 302
        # and a location header redirecting to the correct public URL
        if r.status_code != 302:
            logging.error(
                "The private link you gave is not recognized by uMap. If map is private, any edit will fail."
            )
        else:
            # We only keep the "endpoint" component
            self._public_fragment = r.headers["location"].split(
                uMapSession.BASE_API_FRAGMENT
            )[1]
            # Needed to get CSRF
            self._session.get(self._public_fragment)
            logging.info(f"Private link works and redirects to {self.public_url}.")

    @classmethod
    def from_scratch(cls, instance_url: str = None):
        """
        Initialize a uMap Client by creating a new map.

        instance_url is the uMap base URL, default OSM.
        """
        if instance_url is None:
            instance_url = cls.DEFAULT_INSTANCE

        new_map_url = cls._create_map(instance_url)
        return cls(new_map_url)

    @property
    def map_id(self) -> str:
        if id_match := search(self._ID_REGEXP, self.private_url):
            return id_match.group(2)
        raise ValueError(f"Map ID not found in private URL {self.private_url}")

    @property
    def public_url(self) -> str:
        """Get the public URL, editable if appropriate permissions has been set."""
        return f"{self._instance_url}/{uMapSession.BASE_API_FRAGMENT}{self._public_fragment}"

    @property
    def private_url(self) -> str:
        """Get a private link, authorized for edition."""
        if not self._private_fragment:
            return None
        return f"{self._instance_url}/{uMapSession.BASE_API_FRAGMENT}/{self._private_fragment}"

    @property
    def layers(self) -> List[uMapLayer]:
        """Get existing map layers."""
        # uMap is designed with a partial API. Information about
        # layers is only sent in a JSON files, within a script,
        # within a dynamically generated HTML. Simplest way is
        # to use a regexp for now.
        page = self._session.get(self._public_fragment).text
        layers = []
        if layer_match := search(self._LAYERS_REGEXP, page):
            layers_json = json_load(layer_match.group(1))
            for layer in layers_json:
                layers.append(
                    uMapLayer(
                        layer["name"], int(layer["id"]), bool(layer["displayOnLoad"])
                    )
                )
        return layers

    @property
    def edit_permission(self) -> uMapAnonymousEdit:
        page = self._session.get(self._public_fragment).text
        if match := search(self._EDIT_STATUS_REGEXP, page):
            return match.group(1)
        logging.error(
            "Cannot extract edit permissions from uMap, this is probably a bug."
        )
        return None

    @edit_permission.setter
    def edit_permission(self, edit_permission: uMapAnonymousEdit) -> None:
        """Update edit permissions : anyone or private (i.e. needs a private URL for editing)."""
        form_params = {"edit_status": (None, edit_permission.value)}
        logging.debug(f"Updating map permissions with params {form_params}...")
        self._session.post(
            f"{self.map_id}/update/permissions/",
            files=form_params,
        )
        logging.debug("Map permissions successfully updated.")

    @property
    def settings(self) -> uMapSettings:
        """Get settings of the map, limited to those editable by this client."""
        page = self._session.get(self._public_fragment).text
        _settings = uMapSettings()
        if match := search(self._PROPERTIES_REGEXP, page):
            # Regexp only catches a partial JSON object, close it
            json_settings = json_load(match.group(1) + "}")
            _settings.map_name = json_settings["name"]
        else:
            logging.error("Cannot extract settings from uMap, this is probably a bug.")
            return None
        if match := search(self._CENTER_REGEXP, page):
            json_center: List[float] = json_load(match.group(1))
            _settings.map_center = (json_center[0], json_center[1])
            return _settings
        logging.error("Cannot extract center from uMap, this is probably a bug.")
        return None

    @settings.setter
    def settings(self, settings: uMapSettings) -> None:
        """Update map name and center, with default UI settings."""
        form_params = self._get_settings_form_data(settings)
        logging.debug(f"Updating map settings with params {form_params}...")
        self._session.post(f"{self.map_id}/update/settings/", files=form_params).json()
        logging.debug("Map settings successfully updated.")

    def create_layer(self, name: str, *, display_on_load: bool = True) -> int:
        """
        Create a new layer on the map with uMap defaults.
        Returns the identifier of the newly created layer.

        * name is the name of the label
        * display_on_load controls the visibility of the layer when the map loads.
        """
        dumb_geojson = FeatureCollection([])
        dumb_geojson["_umap_options"] = {
            "displayOnLoad": display_on_load,
            "browsable": True,
            "remoteData": {},
            "name": name,
        }
        form_data = {
            "name": (None, name),
            "display_on_load": (None, True),
            # Seems that rank auto-increment
            "rank": (None, len(self.layers)),
            "geojson": geojson_dumps(dumb_geojson),
        }
        logging.debug("Trying to create layer...")
        r = self._session.post(
            f"{self.map_id}/datalayer/create/",
            files=form_data,
        ).json()
        layer_id = r["id"]
        logging.debug(f"Layer successfully created with id {layer_id}")
        return layer_id

    def delete_layer(self, layer_id: int) -> None:
        """Delete a layer on the map."""
        layer = self._find_layer(layer_id)
        self._session.post(f"{self.map_id}/datalayer/delete/{layer.identifier}/")
        logging.debug(f"Deleted layer with id {layer_id}.")

    def import_features(
        self,
        umap_features: List[uMapFeature],
        *,
        layer_id: int = None,
        name: str = "geojson",
    ):
        """
        Import GeoJSON features into the map.

        The label of a point can be set within the `name` field of a Feature property.
        * features is a list of GeoJSON features with uMap cosmetic properties
          which will be integrated into a FeatureCollection
        * layer_id is the identifier of the layer to import to.
          if not specified, create a new layer with the GeoJSON filename.
        * name is the name of the layer, if a new layer

        # TODO Caution : layer will be overwritten, otherwise we would need
        to manually merge actual data with new data, overkill for our usage.
        """

        features: List[Feature] = []
        for umap_feature in umap_features:
            umap_options = {
                "iconClass": umap_feature.icon.value,
                "showLabel": umap_feature.show_label,
                "color": umap_feature.color.value,
            }
            # _umap_options is a special field used by uMap to store visual appearance
            umap_feature.feature["properties"]["_umap_options"] = umap_options
            features.append(umap_feature.feature)

        if not layer_id:
            layer_id = self.create_layer(name)
        # Overwrite the existing layer
        layer = self._find_layer(layer_id)
        form_data = {
            "name": (None, layer.name),
            "display_on_load": (None, layer.displayOnLoad),
            # Actual rank is not returned by the API...
            "rank": (None, 0),
            "geojson": geojson_dumps(FeatureCollection(features)),
        }
        logging.debug("Trying to overwrite layer with new data.")
        self._session.post(
            f"{self.map_id}/datalayer/update/{layer_id}/",
            files=form_data,
        )
        logging.debug("Layer successfully updated")

    def _find_layer(self, layer_id: int) -> uMapLayer:
        """Return the complete uMapLayer from a layer_id if it exists on the map."""
        try:
            layer = next(l for l in self.layers if l.identifier == layer_id)
            logging.debug(f"Layer with id {layer_id} found.")
            return layer
        except StopIteration as e:
            raise ValueError(f"Data layer {layer_id} does not exist on the map") from e

    @classmethod
    def _create_map(cls, instance_url: str) -> str:
        """Create an empty map with default settings and return private link"""
        form_params = cls._get_settings_form_data(uMapSettings())
        logging.debug(f"Creating new map with settings {form_params}...")

        session = uMapSession(instance_url)
        # We first need to get cookies and CSRF as if we consulted
        # the web UI for creating a new map
        session.get("new/")

        # uMap API uses multipart/form-data in POST requests,
        # probably to be able to send large quantities of data,
        # hence the `files` parameter.
        r = session.post("create/", files=form_params).json()
        private_url = r["permissions"]["anonymous_edit_url"].replace('http://', 'https://')

        logging.info("Map successfully created.")
        logging.warning(
            f"Map will only be editable with a private link. Please save it for later use : {private_url}"
        )
        return private_url

    @classmethod
    def _get_settings_form_data(cls, settings: uMapSettings) -> Dict:
        """
        Build and return a dictionary with all the needed parameters
        to update a map's settings.

        Values are string so they can be sent as `multipart/form-data`,
        which requires string or binary.
        """
        # See https://stackoverflow.com/a/12385661 for context
        return {
            "name": (None, settings.map_name),
            "center": (None, cls._json_to_txt(Point(settings.map_center))),
            "settings": (
                None,
                cls._json_to_txt(
                    Feature(
                        geometry=Point(settings.map_center),
                        properties={
                            **cls.DEFAULT_PROPERTIES,
                            "name": settings.map_name,
                        },
                    )
                ),
            ),
        }

    @staticmethod
    def _json_to_txt(json: Dict) -> str:
        """Convert a dict to str with no spaces."""
        return json_dumps(json, separators=(",", ":"))
