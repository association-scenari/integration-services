"""
Dolibarr-related utilities.
"""
from datetime import date

from .api import APIFailure, APISession

class DolibarrClient:
    """
    Simple wrapper around the subset of the Dolibarr API
    that we use for synchronization.

    It makes code more readable by hiding requests calls and
    API endpoints.

    Each instance is bound to an authenticated session.
    """

    HEADER_TOKEN_NAME = "DOLAPIKEY"
    BASE_API_FRAGMENT = "api/index.php"

    def __init__(self, instance_url: str, api_token: str):
        """Initialize an authenticated session."""
        auth_header = {self.HEADER_TOKEN_NAME: api_token}
        self._session = APISession(instance_url, self.BASE_API_FRAGMENT, headers=auth_header)
        # Reference date to find still active members
        self._today: str = date.today().strftime("%Y/%m/%d")

    def _get_members(self, page: int, sql_filter: str) -> list[dict]:
        """Get 100 members starting from member n°`page*100`, in ascending order, with optional filtering."""
        try:
            params = {"page": page, "sortfield": "t.rowid", "sortorder": "ASC"}
            if sql_filter:
                params["sqlfilters"] = sql_filter
            return self._session.get("members", params=params).json()
        except APIFailure as e:
            # If `page` > real page number, Dolibarr throws a 404
            if e.response.status_code == 404:
                return []
            raise e

    def get_all_members(self, sql_filter: str = None) -> list[dict]:
        """
        Fetch all matching members without no pagination.

        sql_filter is documented there : https://wiki.dolibarr.org/index.php?title=Module_Web_Services_API_REST_(developer)#sqlfilters
        It can apply on fields documented there : https://wiki.dolibarr.org/index.php?title=Table_llx_adherent
        """
        all_members: list[dict] = []
        page: int = 0
        current_page: list[dict] = self._get_members(page, sql_filter)
        while current_page:
            all_members.extend(current_page)
            page = page + 1
            current_page = self._get_members(page, sql_filter)
        return all_members

    def get_active_members(self) -> list[dict]:
        """Fetch all active members, i.e. subscription not ended nor cancelled."""
        # Assume PostgreSQL, explicit cast needed
        active_filter = f"(t.datefin:>:TO_TIMESTAMP('{self._today}', 'YYYY/MM/DD'))"
        return self.get_all_members(active_filter)

    def update_member(self, member_id: int, updates: dict) -> list[dict]:
        """
        Update properties for a specific member.
        
        The Swagger API explorer is not very explicit about the request structure, but it is simple.
        updates is a dictionary with the fields you want to update. 
        The structure is the same that the one returned by get*members calls, including nested dictionaries.
        """
        return self._session.put(f"members/{member_id}", json=updates)