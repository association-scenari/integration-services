#!/usr/bin/env python
"""
This script helps to link Dolibarr adherents and Discourse users.
To do so, it matches adherents and users by email. 
For each match, it sets the « Pseudo forum » custom Dolibarr field 
to be the Discourse username.
"""
import logging
import time

from argparse import ArgumentParser, RawTextHelpFormatter
from collections import defaultdict
from textwrap import dedent

import progressbar

from clients.dolibarr import DolibarrClient
from clients.discourse import DiscourseClient
import utils

def build_parser() -> ArgumentParser:
    """Gets a parser allowing to configure log level"""
    parser = ArgumentParser(
        description="Sets « Pseudo forum » field on Dolibarr for each matching user on Discourse.",
        epilog=dedent(
            f"""
            Instances configuration is passed through environment variables.
            * {utils.ENV_DOLIBARR_INSTANCE_URL} is the base URL of your Dolibarr instance
            * {utils.ENV_DOLIBARR_API_KEY} is the API key for accessing your Dolibarr members
            * {utils.ENV_DISCOURSE_INSTANCE_URL} is the base URL of your Discourse instance
            * {utils.ENV_DISCOURSE_API_KEY} is the API key for accessing your Discourse users
            """
        ),
        formatter_class=RawTextHelpFormatter,
    )
    utils.add_log_level(parser)
    return parser

def main():
    # Get CLI arguments and setup logging
    parser = build_parser()
    args = parser.parse_args()
    utils.configure_logger(args.log_level)
    
    # Allow usage of progress bar and logging, see https://progressbar-2.readthedocs.io/en/latest/#progressbars-with-logging
    progressbar.streams.wrap_stderr()

    # Create a Dolibarr API client
    dolibarr_instance_url: str = utils.get_env(utils.ENV_DOLIBARR_INSTANCE_URL)
    dolibarr_api_key: str = utils.get_env(utils.ENV_DOLIBARR_API_KEY)
    dolibarr_client = DolibarrClient(dolibarr_instance_url, dolibarr_api_key)

    # Create a Discourse API client
    discourse_instance_url: str = utils.get_env(utils.ENV_DISCOURSE_INSTANCE_URL)
    discourse_api_key: str = utils.get_env(utils.ENV_DISCOURSE_API_KEY)
    discourse_client = DiscourseClient(discourse_instance_url, discourse_api_key)

    adherents = dolibarr_client.get_active_members()
    logging.info(f"{len(adherents)} active adherents found on Dolibarr")
    adherents_by_email = defaultdict(list)
    adherent_by_id = {}
    # Take care of shared emails, could happen
    for a in adherents:
        adherents_by_email[a["email"]].append(a["id"])
        adherent_by_id[a["id"]] = a

    members = discourse_client.get_active_users()
    logging.info(f"{len(members)} active users found on Discourse")
    # For each Discourse member, check if an adherent has the same email. 
    # If so, set the pseudoforum field automagically.
    n_updates = 0
    for m in progressbar.progressbar(members):
        email = m["email"]
        if email in adherents_by_email:
            # Handling shared email
            for adherent_id in adherents_by_email[email]:
                adherent = adherent_by_id[adherent_id]
                pseudo_discourse = m["username"]
                pseudo_dolibarr = adherent["array_options"]["options_pseudoforum"]
                # Replace only if mandatory, useful for a cron job
                if not pseudo_dolibarr or pseudo_dolibarr.lower() != pseudo_discourse.lower():
                    # This is the internal structure to describe custom fields
                    adherent["array_options"].update({"options_pseudoforum": pseudo_discourse})
                    updates = {"array_options": adherent["array_options"]}
                    logging.debug(f'Replacing {adherent_id} pseudoforum (actual one is «{pseudo_dolibarr}») with «{pseudo_discourse}»...')
                    n_updates += 1
                    dolibarr_client.update_member(adherent_id, updates)
        # Useless waiting time so we have a nice animated progress bar :)
        time.sleep(0.001)

    logging.info(f'{n_updates} updates done on Dolibarr. Exiting !')

if __name__ == "__main__":
    main()