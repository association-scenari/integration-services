#!/usr/bin/env python
"""
This scripts tries to attribute a custom badge along with a group
on a Discourse instance based on active members of a Dolibarr instance.

Matching is done by email.
"""

import logging

from argparse import ArgumentParser, RawTextHelpFormatter

from clients.dolibarr import DolibarrClient
import utils

dolibarr_instance_url = utils.get_dolibarr_instance_url()
dolibarr_api_key = utils.get_dolibarr_api_key()
dolibarr_client = DolibarrClient(dolibarr_instance_url, dolibarr_api_key)
members = dolibarr_client.get_all_members()


discourse_api_key = utils.get_discourse_api_key()

ENV_GROUP_NAME = 'GROUP_NAME'
def build_parser() -> ArgumentParser:
    """
    Gets a parser allowing configure log level, group and
    badge names, and synchronization of deletions.
    """
    parser = ArgumentParser(
        description="Assign a group and/or a custom badge to Discourse users by matching them with active members of a Dolibarr instance.",
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument(
        "-g",
        "--group",
        help="Optional name of group to assign to active members",
        action=utils.env_default(ENV_GROUP_NAME)
    )
    #TODO
    pass
