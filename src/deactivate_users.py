#!/usr/bin/env python
"""
Allows for bulk-deactivation of users by email, e.g. following a mass sending.
"""
import logging
import progressbar
import time

from argparse import ArgumentParser, RawTextHelpFormatter
from textwrap import dedent

from clients.discourse import DiscourseClient
import utils


def build_parser() -> ArgumentParser:
    """Gets a parser allowing to configure log level"""
    parser = ArgumentParser(
        description="Deactivate a list of users based on email adress.",
        epilog=dedent(
            f"""
            Instances configuration is passed through environment variables.
            * {utils.ENV_DISCOURSE_INSTANCE_URL} is the base URL of your Discourse instance
            * {utils.ENV_DISCOURSE_API_KEY} is the API key for accessing your Discourse users
            """
        ),
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument(
        "input", help="Path to a file containing one email address per line", type=str
    )
    utils.add_log_level(parser)
    return parser


def main():
    # Get CLI arguments and setup logging
    parser = build_parser()
    args = parser.parse_args()
    utils.configure_logger(args.log_level)

    # Allow usage of progress bar and logging, see https://progressbar-2.readthedocs.io/en/latest/#progressbars-with-logging
    progressbar.streams.wrap_stderr()

    # Create a Discourse API client
    discourse_instance_url: str = utils.get_env(utils.ENV_DISCOURSE_INSTANCE_URL)
    discourse_api_key: str = utils.get_env(utils.ENV_DISCOURSE_API_KEY)
    discourse_client = DiscourseClient(discourse_instance_url, discourse_api_key)

    # Loop through mails
    with open(args.input, "r") as f:
        emails = list(f)
        for email in progressbar.progressbar(emails):
            accounts = discourse_client.get_active_users({"email": email.strip()})
            for target in accounts:
                discourse_client.deactivate_user(target["id"])
            # Rate-limiting
            time.sleep(1)


if __name__ == "__main__":
    main()
