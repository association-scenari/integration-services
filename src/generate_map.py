#!/usr/bin/env python
"""
This script localize members of a Dolibarr instance
on a private uMap map, which can later be integrated
e.g. in an iframe.

It takes care of creating the map if it does not exists,
but can also edit an existing map.

It localize members at town level thanks to Nominatim and
handles especially well french edge-cases.
"""
import logging

from argparse import ArgumentParser, RawTextHelpFormatter
from collections import defaultdict
from dataclasses import dataclass
from enum import Enum
from json import loads as json_load
from textwrap import dedent
from time import sleep
from typing import DefaultDict, Dict, List

from geojson import Point, Feature
from geopy import geocoders
import progressbar

from clients.dolibarr import DolibarrClient

import clients.umap as umap
import utils

NOMINATIM_SECONDS_DELAY = 1
# Some requests are very long, esp. for counties and countires
NOMINATIM_TIMEOUT = 20
NOMINATIM_USER_AGENT = "scenari-dolibarr-localizator"
FR_COUNTRY_CODE = "FR"

# Load postal/insee code data
with open("./data/cedex_insee.json", mode="r", encoding="utf-8") as f:
    cedex_insee = json_load(f.read())
with open("./data/insee_postal.json", mode="r", encoding="utf-8") as f:
    insee_postal = json_load(f.read())


class LocationTypeColor(Enum):
    """
    All locations determined by Nominatim do not have the same meaning.
    Although they are coordinates, they reprensent different "level" of locations.
    We assign a different color to point to give an idea of the precision.
    """

    TOWN = umap.uMapColor.GREEN
    COUNTY = umap.uMapColor.BLUE
    COUNTRY = umap.uMapColor.RED


# eq and frozen set to true generate a __hash__ method.
# Allows for comparing objects, removing duplicate, etc.
@dataclass(eq=True, frozen=True)
class Location:
    longitude: str
    latitude: str
    location_type: LocationTypeColor


def build_parser() -> ArgumentParser:
    #FIXME do not use environment for non-secret stuff
    """Gets a parser allowing to limit countries and configure log level"""
    parser = ArgumentParser(
        description="Generates a map which show the locations where active members of a Dolibarr instance live.",
        epilog=dedent(
            f"""
            Instances configuration is passed through environment variables.
            * {utils.ENV_DOLIBARR_INSTANCE_URL} is the base URL of your Dolibarr instance
            * {utils.ENV_DOLIBARR_API_KEY} is the API key for accessing your Dolibarr members
            * (optional) {utils.ENV_UMAP_PRIV_URL} is an editable private URL of an existing map
            * (optional) {utils.ENV_UMAP_NAME} is the name of the map

            On the map, colors are represent the precision level of geocoding.
            * {LocationTypeColor.TOWN.value.value} means town level
            * {LocationTypeColor.COUNTY.value.value} means county level
            * {LocationTypeColor.COUNTRY.value.value} means country level
            """
        ),
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument(
        "-c",
        "--countries",
        help="List of ISO 3166 countries codes or 'all' to include, separated by commas (default: all)",
        # Get a list out of comma-separated countries
        type=lambda countries: [c.upper() for c in countries.split(",")],
    )
    parser.add_argument(
        "-x",
        "--exclude",
        help="List of ISO 3166 countries codes or 'all' to exclude, separated by commas (default: none)",
        type=lambda countries: [c.upper() for c in countries.split(",")],
    )
    utils.add_log_level(parser)
    return parser


def get_french_postal_code(zip_code: str) -> str:
    """
    From a zip code (which can be cedex or not), get a
    localizable postal code or None.
    """
    try:
        logging.debug(f"Looking for code {zip_code}...")
        insee_code = next(c["insee"] for c in cedex_insee if c["code"] == zip_code)
        logging.debug(f"Found corresponding INSEE code : {insee_code}.")
        # Then, find a suitable postal code for the INSEE code
        # and replace user values by "official" values to help Nominatim
        postal_code = next(
            c["fields"]["code_postal"]
            for c in insee_postal
            if c["fields"]["code_commune_insee"] == insee_code
        )
        logging.debug(f"Resulting postal code is {postal_code}")
        return postal_code
    # In that case the zip code is probably incorrect.
    except StopIteration:
        logging.debug(f"FR {zip_code} not recognized in the INSEE database")
        return None


def localize_members(members: List[Dict]) -> DefaultDict[Point, List[str]]:
    """
    From a list of Dolibarr members (API format), returns a
    dict indexed by town 2D coordinates and valued by a list
    of members ids within the town.
    """

    # Used to map town locations to member ids
    # e.g. which members are in Compiegne, etc.
    locations_member_ids: DefaultDict[Location, List[str]] = defaultdict(list)
    if not members:
        return locations_member_ids

    # Create a Nominatim API client
    # We need to tell who we are, hence the custom user-agent.
    # See https://operations.osmfoundation.org/policies/nominatim/
    nominatim_client = geocoders.Nominatim(
        user_agent=NOMINATIM_USER_AGENT, timeout=NOMINATIM_TIMEOUT
    )

    failed_ids: List[int] = []
    for member in progressbar.progressbar(members):
        # We only get postal code, which is sufficient to get a reasonable
        # geographic estimation. Even if several towns can have the same
        # postal code, the goal is to locate most members, not to be
        # as precise as possible. And towns can be ill-written, which
        # would reduce the number of members taken into account.
        member_id = member.get("id")
        zip_code = member.get("zip")
        county = member.get("state_id")
        country = member.get("country_code")
        logging.debug(f"Processing member with ID {member_id}...")

        # Nominatim does not supports postalcode + county,
        # prioritize zip code which is more precise.
        # Fallback to other options if zip does not works.
        # `geocode` gets a single point centered on the town.
        result = None
        if zip_code:
            location_type = LocationTypeColor.TOWN
            result = _geocode(
                nominatim_client, {"postalcode": zip_code}, country=country
            )
            # Failure could be because of CEDEX stuff, not localizable on Nominatim.
            # In that case, try to find a suitable postal code and start again.
            if result is None and country == FR_COUNTRY_CODE:
                zip_code = get_french_postal_code(zip_code)
                if zip_code is not None:
                    result = _geocode(
                        nominatim_client, {"postalcode": zip_code}, country=country
                    )
        if county and result is None:
            location_type = LocationTypeColor.COUNTY
            result = _geocode(nominatim_client, {"county": county}, country=country)
        if country and result is None:
            location_type = LocationTypeColor.COUNTRY
            result = _geocode(nominatim_client, {"country": country}, country=country)
        if result is None:
            logging.warning(f"Geocoding failed for member with ID {member_id}.")
            failed_ids.append(member_id)
        else:
            location = Location(result.longitude, result.latitude, location_type)
            # Finally, add member id to location if it already exists, or create a new key.
            # Note that because we only use town, all "Compiegne" lat/lon will be equal.
            # It works because Location implements __eq__ and __hash__.
            locations_member_ids[location].append(member_id)
        # To enforce the Nominatim Usage Policy, we wait between successive requests.
        sleep(NOMINATIM_SECONDS_DELAY)

    if failed_ids:
        logging.error(
            f"Geocoding failed for {len(failed_ids)} members. IDs: {failed_ids}"
        )

    return locations_member_ids


def update_or_create_map(features: List[umap.uMapFeature]) -> str:
    """
    Update or create an uMap map with a GeoJSON feature list.

    A new map is created or an existing map is used depending
    on the configuration.

    All data is overwritten.

    Returns the public URL of the map.
    """
    # Can be None, which means create a new one or use defaults
    map_private_url = utils.get_env(utils.ENV_UMAP_PRIV_URL, None)
    map_name = utils.get_env(utils.ENV_UMAP_NAME, None)
    settings = umap.uMapSettings(map_name=map_name)
    if map_private_url:
        umap_client = umap.uMapClient(private_url=map_private_url)
    else:
        umap_client = umap.uMapClient.from_scratch()
    umap_client.settings = settings
    umap_client.edit_permission = umap.uMapAnonymousEdit.PRIVATE

    # Delete all existing layers
    for layer in umap_client.layers:
        umap_client.delete_layer(layer.identifier)

    # Import GeoJSON in a new layer
    umap_client.import_features(features)

    return umap_client.public_url


def _geocode(
    nominatim_client: geocoders.Nominatim,
    query: Dict,
    *,
    country: str = None,
) -> Location:
    result = nominatim_client.geocode(query, country_codes=country)
    if result is None:
        logging.debug(f"Nomimatim query failed for {query}, country_codes = {country}.")
    return result


def main():
    # Get CLI arguments and setup logging
    parser = build_parser()
    args = parser.parse_args()
    utils.configure_logger(args.log_level)

    # Allow usage of progress bar and logging, see https://progressbar-2.readthedocs.io/en/latest/#progressbars-with-logging
    progressbar.streams.wrap_stderr()

    # Create a Dolibarr API client
    dolibarr_instance_url: str = utils.get_env(utils.ENV_DOLIBARR_INSTANCE_URL)
    dolibarr_api_key: str = utils.get_env(utils.ENV_DOLIBARR_API_KEY)
    dolibarr_client = DolibarrClient(dolibarr_instance_url, dolibarr_api_key)

    active_members: Dict = dolibarr_client.get_active_members()
    logging.info(f"{len(active_members)} active members found")

    need_include: bool = args.countries and "all" not in args.countries
    need_exclude: bool = args.exclude and "all" not in args.exclude
    if need_include:
        logging.info(f"Including members in {args.countries}...")
    if need_exclude:
        logging.info(f"Excluding members in {args.exclude}...")

    # Start by filtering members by countries
    members_filtered = [
        m
        for m in active_members
        if (not need_include or (m.get("country_code") in args.countries))
        and (not need_exclude or (m.get("country_code") not in args.exclude))
    ]
    logging.info(f"{len(members_filtered)} members with requested countries found")

    # Suboptimal (decomposition and recombination a few lines after)
    members_zip = [m for m in members_filtered if m["zip"]]
    logging.info(f"{len(members_zip)} members with a zip code found")

    members_county = [m for m in members_filtered if not m["zip"] and m["state_id"]]
    logging.info(
        f"{len(members_county)} members without zip code but with county code found"
    )

    members_country = [
        m
        for m in members_filtered
        if not m["zip"] and not m["state_id"] and m["country_code"]
    ]
    logging.info(f"{len(members_country)} members with only country available")

    members_ignored = [
        m["id"]
        for m in members_filtered
        if not m["zip"] and not m["state_id"] and not m["country_code"]
    ]
    logging.warning(
        f"{len(members_ignored)} members ignored with no localization information. IDs : {members_ignored}"
    )

    members_to_locate = members_country + members_county + members_zip
    logging.info("Localizing members...")
    locations_member_ids: DefaultDict[Location, List[str]] = localize_members(
        members_to_locate
    )

    logging.info("Building GeoJSON features...")
    features: List[Feature] = [
        umap.uMapFeature(
            Feature(
                geometry=Point((location.longitude, location.latitude)),
                properties={"name": f'{" - ".join(ids)}'},
            ),
            icon=umap.uMapIcon.CIRCLE,
            show_label=False,
            color=location.location_type.value,
        )
        for location, ids in locations_member_ids.items()
    ]

    logging.info("Importing data on uMap...")
    public_url = update_or_create_map(features)
    logging.info(f"Map generated successfully. See it at {public_url}")


if __name__ == "__main__":
    main()
