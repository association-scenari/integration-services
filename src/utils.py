import logging

from argparse import ArgumentParser, Action
from os import environ

# Environment variables names
#FIXME put in respective scripts
ENV_DOLIBARR_API_KEY = "DOLIBARR_API_KEY"
ENV_DOLIBARR_INSTANCE_URL = "DOLIBARR_INSTANCE_URL"
ENV_DISCOURSE_API_KEY = "DISCOURSE_API_KEY"
ENV_DISCOURSE_INSTANCE_URL = "DISCOURSE_INSTANCE_URL"
ENV_UMAP_PRIV_URL = "UMAP_PRIVATE_URL"
ENV_UMAP_NAME = "UMAP_NAME"

# To differentiate between None passed
# as an argument and default value
_sentinel = object()

# See https://gist.github.com/orls/51525c86ee77a56ad396
class EnvDefault(Action):
    """An argparse action class that auto-sets missing default values from env
    vars. Defaults to requiring the argument."""

    def __init__(self, envvar, required=True, default=None, **kwargs):
        if not default and envvar:
            if envvar in environ:
                default = environ[envvar]
        if required and default:
            required = False
        super(EnvDefault, self).__init__(default=default, required=required,
                                         **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)

# functional sugar for the above
def env_default(envvar):
    def wrapper(**kwargs):
        return EnvDefault(envvar, **kwargs)
    return wrapper

class MissingEnv(Exception):
    def __init__(self, key):
        super().__init__(f"Missing {key} environment variable")

def add_log_level(parser: ArgumentParser):
    '''
    Add a configurable log level to an argument parser.
    '''
    parser.add_argument(
        "-l",
        "--log-level",
        default="INFO",
        # Trick to convert string to numerical log level
        type=lambda x: getattr(logging, x.upper()),
        help="configure the logging level (DEBUG, INFO(*), WARNING, or ERROR)",
    )

def configure_logger(log_level: str):
    '''Configure the default logger with nice formatting and custom log level.'''
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(asctime)s:%(levelname)-7s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(log_level)

def get_env(key: str, default: str=_sentinel) -> str:
    """
    Returns the value of an environment variable, or a defaut.
    If the variable does not exist and the defaut is not set,
    raise a MissingEnv exception.
    """
    value: str = environ.get(key)
    if value is None:
        if default == _sentinel:
            raise MissingEnv(key)
        return default
    return value

