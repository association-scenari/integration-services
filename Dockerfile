FROM python:3.10-slim-buster

COPY src /src
COPY requirements.txt /src/requirements.txt

WORKDIR /src

RUN pip3 install -r ./requirements.txt
RUN chmod +x *.py